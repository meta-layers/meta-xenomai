DESCRIPTION = "Provides userspace xenomai support and libraries needed to for \
real-time applications using the xenomai RTOS implementation"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://include/COPYING;md5=79ed705ccb9481bf9e7026b99f4e2b0e"
SECTION = "xenomai"
HOMEPAGE = "http://www.xenomai.org/"

SRC_URI = "git://gitlab.denx.de/Xenomai/xenomai.git;protocol=https;branch=master" 

# --> 
# ideally in something like local.conf:
# XENOMAITAG = "5714ceede70ccd39b1bf6c41a72cfa94f98e169a"
# SRCREV_pn-xenomai = "${XENOMAITAG}"
# SRCREV_prepare-kernel = "${XENOMAITAG}"
SRCREV ?= "5714ceede70ccd39b1bf6c41a72cfa94f98e169b"
# <--

PV = "3.1+git${SRCPV}"

S = "${WORKDIR}/git"

inherit autotools pkgconfig

includedir = "/usr/include/xenomai"

PACKAGES += "${PN}-demos"

FILES_${PN}-demos = "/usr/demo"
FILES_${PN}-dev += "/dev"
FILES_${PN} += " \
  /usr/lib/modechk.wrappers \
  /usr/lib/cobalt.wrappers \
  /usr/lib/dynlist.ld \
"
EXTRA_OECONF += "--enable-smp \
                --with-core=cobalt"


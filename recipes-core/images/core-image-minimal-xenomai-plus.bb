DESCRIPTION = "A small image just capable of allowing a device to boot plus a real-time test suite and tools appropriate for real-time use."

require core-image-minimal-xenomai.bb

# let's install various kernels plus modules:
# xenomai-linux is already installed by default

IMAGE_INSTALL += " \
                  prt-linux prt-linux-modules \
                  debug-linux debug-linux-modules \
                  up-linux up-linux-modules \
                  "
# -->
# does not like this for some reason
# see core-image-minimal-xenomai-plus-error.txt
# std-linux std-linux-modules
# <-- 

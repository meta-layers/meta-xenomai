# linux-yocto-custom_x.y.z.bb 
# attempt to have only kernel version related stuff in here

KTYPE="xenomai"

IPIPE_PATCH = "ipipe-core-4.19.128-cip28-arm-10.patch"

# --> 
# ideally in something like local.conf:
# XENOMAITAG = "5714ceede70ccd39b1bf6c41a72cfa94f98e169a"
# SRCREV_pn-xenomai = "${XENOMAITAG}"
# SRCREV_prepare-kernel = "${XENOMAITAG}"
# that's the commit id of the xenomai repo - needs to match
SRCREV_prepare-kernel ?= "5714ceede70ccd39b1bf6c41a72cfa94f98e169ba"
# <--

require recipes-kernel/linux/linux-yocto-custom_${PV}.inc

# we don't want this here:
#require recipes-kernel/linux/linux-yocto-custom-common_4.19.inc

# special stuff for xenomai:
require recipes-kernel/linux/linux-yocto-custom-common-xenomai_4.19.inc

# require xenomai common stuff
require recipes-kernel/linux/linux-yocto-custom-xenomai.inc

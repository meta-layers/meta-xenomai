KTYPE="xenomai"

IPIPE_PATCH = "ipipe-core-4.19.128-cip28-arm-10.patch"

# --> 
# ideally in something like local.conf:
# XENOMAITAG = "5714ceede70ccd39b1bf6c41a72cfa94f98e169a"
# SRCREV_pn-xenomai = "${XENOMAITAG}"
# SRCREV_prepare-kernel = "${XENOMAITAG}"
# that's the commit id of the xenomai repo - needs to match
SRCREV_prepare-kernel ?= "5714ceede70ccd39b1bf6c41a72cfa94f98e169ba"
# <--

# here we put the xenomai patches
XENOPATCHPATH="${THISDIR}/xenopatch/4.19.x"
FILESEXTRAPATHS_prepend := "${XENOPATCHPATH}:"

# xenomai common stuff 

# the ipipe patch is part of our meta data e.g.
# meta-multi-v7-ml-bsp/recipes-kernel/linux/xenopatch/x.y.z
SRC_URI += "file://${IPIPE_PATCH};apply=0"

# --> we want the /scripts/prepare-kernel.sh script, which comes from the xenomai (user space) repo
# for some reason when I include the whole xenomai repo it tries to get the kernel branch,
# so as a workaround I user subpaths, which seems to work
SRC_URI += "git://gitlab.denx.de/Xenomai/xenomai.git;protocol=https;branch=master;subpath=scripts;name=prepare-kernel"
SRC_URI += "git://gitlab.denx.de/Xenomai/xenomai.git;protocol=https;branch=master;subpath=config;name=prepare-kernel"
SRC_URI += "git://gitlab.denx.de/Xenomai/xenomai.git;protocol=https;branch=master;subpath=include;name=prepare-kernel"
SRC_URI += "git://gitlab.denx.de/Xenomai/xenomai.git;protocol=https;branch=master;subpath=kernel;name=prepare-kernel"

SRCREV_FORMAT = "the-kernel_prepare-kernel"

do_configure_prepend () {
    # Prepare kernel
    ${WORKDIR}/scripts/prepare-kernel.sh --arch=${ARCH} --linux=${S} --ipipe="${WORKDIR}/${IPIPE_PATCH}" --default
}
# <-- we want the /scripts/prepare-kernel.sh script, which comes from the xenomai (user space) repo


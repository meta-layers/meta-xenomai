Please note that to build the kernel also some dirs from the xenomai repo are needed.

The kernel recipe has also some parts here:

meta-multi-v7-ml-bsp/recipes-kernel/linux/

you can check/modify the xenomai patched kernel config like this:

bitbake linux-yocto-custom-xenomai -c menuconfig

and if the xenomai kernel is the "main" kernel in the image:

bitbake virtual/kernel -c menuconfig
